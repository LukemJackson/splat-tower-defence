﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gameOverUI;

    public static int health = 100;
    public int startHealth;
    public static int money;
    public int startMoney = 500;
    public static int wave;
    public int startWave = 0;

    public Text moneyText;
    public Text livesText;
    public Text waveText;

    public static bool isGameOver = false;

    public void TakeDamage(int damage)
    {
        health -= damage;
        health = Mathf.Clamp(health, 0, 1000);
        livesText.text = "Lives:\t" + health.ToString();
        if (health <= 0 && !isGameOver)
        {
            GameOver();
        }
    }

    void Start()
    {
        isGameOver = false;
        health = startHealth;
        money = startMoney;
        livesText.text = "Lives: " + health.ToString();
        moneyText.text = "Money: " + money.ToString();
    }

    public void ChangeMoney(int value)
    {
        money += value;
        moneyText.text = "Money:\t" + money.ToString();
    }

    public void GameOver()
    {
        isGameOver = true;
        gameOverUI.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        //if (ScoreManager.isGameOver)
        //{
        //    return;
        //}
        moneyText.text = "Money: " + money.ToString();
        livesText.text = "Lives: " + health.ToString();
        waveText.text = "WAVE NUMBER: " + wave.ToString();
    }
}
