﻿using UnityEngine;
namespace LJ
{
    [System.Serializable]
    public class TowerBlueprint
    {
        public GameObject tower;
        public int cost;
    }
}