﻿using UnityEngine;
using UnityEngine.UI;

namespace LJ
{

    public class Shop : MonoBehaviour
    {
        BuildingManager buildingManager;
        public TowerBlueprint m_Tower01;
        public TowerBlueprint m_Tower02;
        public Text tower01Text;
        public Text tower02Text;


        void Start()
        {
            buildingManager = BuildingManager.instance;
            tower01Text.text = m_Tower01.cost.ToString();
            tower02Text.text = m_Tower02.cost.ToString();
        }

        private void SelectTower(int t_Number)
        {
            if(t_Number == 1)
            {
                buildingManager.SetTowerToSelect(m_Tower01);
            }
            else if (t_Number == 2)
            {
                buildingManager.SetTowerToSelect(m_Tower02);
            }
        }

        public void OnClickTower01()
        {
            SelectTower(1);
        }

        public void OnClickTower02()
        {
            SelectTower(2);
        }
    }
}