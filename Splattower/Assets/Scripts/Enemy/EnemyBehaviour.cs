﻿using UnityEngine;

namespace LJ
{
    public class EnemyBehaviour : MonoBehaviour
    {
        [SerializeField]
        private GameObject deathEffect;

        [SerializeField]
        private int m_Damage = 1;

        [SerializeField]
        private int m_Reward = 10;

        [SerializeField]
        private int m_BaseHealth = 1;
        [SerializeField]
        private int m_HealthPerWaveModifier = 1;
        public int m_Health;

        [SerializeField]
        private float m_BaseSpeed = 10.0f;

        public float m_CurrentSpeed;
        [SerializeField]
        private float m_RotateSpeed = 5.0f;

        private Transform m_Target;
        private int m_WaypointNumber;

        private Vector3 m_Direction;
        private Vector3 m_Rotation;

        // Use this for initialization
        void Awake()
        {
            m_WaypointNumber = 0;
            m_Target = WaypointManager.m_Waypoints[m_WaypointNumber];
            m_CurrentSpeed = m_BaseSpeed;
            m_Health = m_BaseHealth + m_HealthPerWaveModifier * (ScoreManager.wave-1);
        }

        // Update is called once per frame
        void Update()
        {
            if (ScoreManager.isGameOver)
            {
                return;
            }

            RotateToDirection();
            MoveToTarget();

            if (Vector3.Distance(transform.position, m_Target.position) <= 0.3f)
            {
                AssignNextWaypoint();
            }
        }

        public void TakeDamage(int amount)
        {
            m_Health -= amount;
            if(m_Health <= 0)
            {
                EnemyDeath();
            }
        }

        void EnemyDeath()
        {
            GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            ScoreManager.money += m_Reward;
            //do some effects
            Destroy(gameObject);
            return;
        }

        void EnemyReachedEnd()
        {
            ScoreManager.health -= m_Damage;
            //do some effects
            Destroy(gameObject);
            return;
        }

        void RotateToDirection()
        {
            m_Direction = m_Target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(m_Direction);
            m_Rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * m_RotateSpeed).eulerAngles;
            transform.rotation = Quaternion.Euler(0, m_Rotation.y, 0);
        }

        void MoveToTarget()
        {
            m_Direction = m_Target.position - transform.position;
            transform.Translate(m_Direction.normalized * m_CurrentSpeed * Time.deltaTime, Space.World);
        }

        void AssignNextWaypoint()
        {
            if(m_WaypointNumber >= WaypointManager.m_Waypoints.Length - 1)
            {
                EnemyReachedEnd();
                return;
            }
            else
            {
                m_WaypointNumber++;
                m_Target = WaypointManager.m_Waypoints[m_WaypointNumber];
            }
        }
    }
}