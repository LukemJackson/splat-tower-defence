﻿using UnityEngine;
using System.Collections;

namespace LJ
{
    public class WaypointManager : MonoBehaviour
    {

        public static Transform[] m_Waypoints;

        // Use this for initialization
        void Awake()
        {
            m_Waypoints = new Transform[transform.childCount];
            for(int i = 0; i < m_Waypoints.Length; i++)
            {
                m_Waypoints[i] = transform.GetChild(i);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}


