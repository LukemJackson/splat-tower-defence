﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace LJ
{
    public class SpawnManager : MonoBehaviour
    {

        [SerializeField]
        private Text TimerText;

        private bool m_IsWaveActive = false;

        // spawnPoint for enemies
        [SerializeField]
        private Transform m_EnemySpawn;

        // enemy types
        [SerializeField]
        private Transform m_EnemyType01;
        [SerializeField]
        private Transform m_EnemyType02;

        // spawning variables
        [SerializeField]
        private float m_EnemySpacingTime = 1.0f;

        [SerializeField]
        public float m_WaveStartTimer = 5.0f;

        [SerializeField]
        public int m_CurrentWave = 0;

        private float m_Countdown;

        void Awake()
        {
            m_Countdown = m_WaveStartTimer;
        }

        void Update()
        {
            if (ScoreManager.isGameOver)
            {
                return;
            }

            if (m_Countdown <= 0f && m_IsWaveActive == false)
            {
                m_IsWaveActive = true;
                StartCoroutine(SpawnWave());
            }
            else if (m_IsWaveActive == false)
            {
                //TimerText.text = "Wave\n"+ (m_CurrentWave+1) + "\nStarts in\n" + Mathf.Floor(m_Countdown + 1f).ToString();
                TimerText.text = string.Format("Wave\n{0:000}\nStarts\nin\n{01}", (m_CurrentWave + 1), Mathf.Floor(m_Countdown + 1f).ToString());
                m_Countdown -= Time.deltaTime;
            }
            else
            {
                TimerText.text = "";
            }

        }

        IEnumerator SpawnWave()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    yield break;
            //}
            m_CurrentWave++;
            ScoreManager.wave = m_CurrentWave;
            for (int i = 0; i < m_CurrentWave; i++)
            {
                SpawnEnemy(i+1);
                yield return new WaitForSeconds(m_EnemySpacingTime);
            }
            m_IsWaveActive = false;
            m_Countdown = m_WaveStartTimer;
        }

        void SpawnEnemy(int number)
        {
            if (number%3==0)
            {
                Instantiate(m_EnemyType02, m_EnemySpawn.position, m_EnemySpawn.rotation);
            }

            else
            {
                Instantiate(m_EnemyType01, m_EnemySpawn.position, m_EnemySpawn.rotation);
            }

        }



    }
}


