﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace LJ
{
    public class Node : MonoBehaviour
    {
        [SerializeField]
        private Color m_HighlightColor;
        [SerializeField]
        private Color m_TooExpensiveColor;
        private Color m_InitialColor;

        [Header("Optional Preplaced Tower")]
        [SerializeField]
        public GameObject m_Tower;

        BuildingManager buildingManager;
        private Renderer m_Renderer;

        void Start()
        {
            m_Renderer = GetComponent<Renderer>();
            m_InitialColor = m_Renderer.material.color;
            buildingManager = BuildingManager.instance;
        }

        void OnMouseEnter()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    return;
            //}

            if (!buildingManager.CanBuild)
            {
                return;
            }

            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            if (buildingManager.CanBuy)
            {
                m_Renderer.material.color = m_HighlightColor;
            }
            else
            {
                m_Renderer.material.color = m_TooExpensiveColor;
            }

            
        }

        void OnMouseExit()
        {
            m_Renderer.material.color = m_InitialColor;
        }

        void OnMouseDown()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    return;
            //}

            if (!buildingManager.CanBuild)
            {
                return;
            }

            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }




            if (m_Tower != null)
            {
                //open ui for active turret
            }

            buildingManager.BuildTowerOn(this);
        }
    }
}