﻿using UnityEngine;
using System.Collections;

namespace LJ
{
    public class TurretBehaviour : MonoBehaviour
    {

        // turret stats
        [Header("Stats")]
        public float m_Range = 10.0f;

        [SerializeField]
        private float m_FireRate = 5.0f;
        private float m_FireTimer = 0f;

        [Header("")]
        [SerializeField]
        private float m_GetTargetFrequency = 0.5f;
        [SerializeField]
        private float m_RotateSpeed = 10f;
        [SerializeField]
        private Transform m_BulletSpawn;

        [SerializeField]
        private GameObject m_Bullet01;

        private Vector3 m_Direction;
        private Vector3 m_Rotation;

        [SerializeField]
        private Transform m_Pivot;


        private Transform m_Target;
        [SerializeField]
        private string m_EnemyTag = "Enemy";
        private GameObject[] m_EnemyArray;


        // Use this for initialization
        void Start()
        {
            InvokeRepeating("GetTarget", 0f, m_GetTargetFrequency);
        }

        // Update is called once per frame
        void Update()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    return;
            //}

            if (m_Target == null)
            {
                //idle spinning
            }
            else
            {
                RotateToDirection();
                if (m_FireTimer <= 0f)
                {
                    Shoot();
                    m_FireTimer = 1f / (m_FireRate / 100);
                }
                m_FireTimer -= Time.deltaTime;




            }
        }

        void Shoot()
        {
            GameObject bulletGO = (GameObject)Instantiate(m_Bullet01, m_BulletSpawn.position, m_BulletSpawn.rotation);
            BulletBehaviour bullet = bulletGO.GetComponent<BulletBehaviour>();

            if (bullet != null)
            {
                bullet.GetTarget(m_Target);
            }
        }

        void GetTarget()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    return;
            //}
            GameObject[] m_EnemyArray = GameObject.FindGameObjectsWithTag(m_EnemyTag);
            float t_ShortestDistance = Mathf.Infinity;
            GameObject t_NearestEnemy = null;
            for (int i = 0; i < m_EnemyArray.Length; i++)
            {
                float t_DistanceToEnemy = Vector3.Distance(transform.position, m_EnemyArray[i].transform.position);
                if (t_DistanceToEnemy < t_ShortestDistance)
                {
                    t_ShortestDistance = t_DistanceToEnemy;
                    t_NearestEnemy = m_EnemyArray[i];
                }
            }


            if (t_NearestEnemy != null && t_ShortestDistance <= m_Range)
            {
                m_Target = t_NearestEnemy.transform;
            }
            else
            {
                m_Target = null;
            }
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, m_Range);
        }


        void RotateToDirection()
        {
            m_Direction = m_Target.position - m_Pivot.transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(m_Direction);
            m_Rotation = Quaternion.Lerp(m_Pivot.rotation, lookRotation, Time.deltaTime * m_RotateSpeed).eulerAngles;
            m_Pivot.rotation = Quaternion.Euler(0, m_Rotation.y, 0);
        }
    }


}
