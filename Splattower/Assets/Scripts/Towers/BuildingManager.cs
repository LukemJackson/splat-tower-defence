﻿using UnityEngine;
using System.Collections;

namespace LJ
{
    public class BuildingManager : MonoBehaviour
    {
        public static BuildingManager instance;
        public bool CanBuild { get { return m_TowerToBuild.tower != null; } }
        public bool CanBuy { get { return ScoreManager.money >= m_TowerToBuild.cost; } }

        [SerializeField]
        private TowerBlueprint m_TowerToBuild;

        //building
        [SerializeField]
        private GameObject m_BuildEffect;
        // towers
        [SerializeField]
        public GameObject m_Tower01;
        [SerializeField]
        public GameObject m_Tower02;

        void Awake()
        {
            instance = this;
        }


        public void SetTowerToSelect(TowerBlueprint t_Tower)
        {
            m_TowerToBuild = t_Tower;   // tower to build is set to the tower clicked on in the UI
        }

        public void BuildTowerOn(Node node)
        {
            if (ScoreManager.money < m_TowerToBuild.cost)
            {
                Debug.Log("Not enough money");
                return;
            }

            ScoreManager.money -= m_TowerToBuild.cost;
            GameObject tower = (GameObject)Instantiate(m_TowerToBuild.tower, node.transform.position, Quaternion.identity); // create selected tower on selected node
            GameObject buildEffect = (GameObject)Instantiate(m_BuildEffect, node.transform.position, Quaternion.identity);  // adds a particle system at location tower is built
            Destroy(buildEffect, 5f); // removes particle system after 5 seconds
            node.m_Tower = tower;
        }
    }
}