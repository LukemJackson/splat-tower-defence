﻿using UnityEngine;
using System.Collections;

namespace LJ
{
    public class BulletBehaviour : MonoBehaviour
    {

        [SerializeField]
        private float m_CurrentSpeed = 10.0f;
        [SerializeField]
        private float m_RotateSpeed = 5.0f;

        [SerializeField]
        private int m_Damage = 1;

        [SerializeField]
        private GameObject HitEffect;

        private Transform m_Target;
        private Vector3 m_Direction;
        private Vector3 m_Rotation;

        // Update is called once per frame
        void Update()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    Destroy(gameObject);
            //    return;
            //}

            if (m_Target == null)
            {
                Destroy(gameObject);
                return;
            }
            RotateToDirection();
            MoveToTarget();
        }

        public void GetTarget(Transform t_Target)
        {
            m_Target = t_Target;
        }

        void RotateToDirection()
        {
            m_Direction = m_Target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(m_Direction);
            m_Rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * m_RotateSpeed).eulerAngles;
            transform.rotation = Quaternion.Euler(0, m_Rotation.y, 0);
        }

        void MoveToTarget()
        {
            m_Direction = m_Target.position - transform.position;
            float t_DistanceThisFrame = m_CurrentSpeed * Time.deltaTime;
            if (m_Direction.magnitude <= t_DistanceThisFrame)
            {
                // enemy hit!
                TargetHit(m_Target);
                return;
            }
            transform.Translate(m_Direction.normalized * m_CurrentSpeed * Time.deltaTime, Space.World);
        }

        void TargetHit(Transform m_Target)
        {
            EnemyBehaviour t_Enemy = m_Target.GetComponent<EnemyBehaviour>();
            if(t_Enemy != null)
            {
                t_Enemy.TakeDamage(m_Damage);
            }
            
            if (HitEffect != null)
            {
                Instantiate(HitEffect, new Vector3(transform.position.x, 0.3f, transform.position.z), transform.rotation);
            }

            Destroy(gameObject);
            return;
        }
    }
}