﻿using UnityEngine;

namespace LJ
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private float m_PanSpeed = 30f;

        [SerializeField]
        private float m_MinX = 15f;
        [SerializeField]
        private float m_MaxX = 15f;

        [SerializeField]
        private float m_MinZ = 15f;
        [SerializeField]
        private float m_MaxZ = 15f;

        [SerializeField]
        private float m_ScrollSpeed = 5f;

        [SerializeField]
        private float m_MinY = 5f;

        [SerializeField]
        private float m_MaxY = 35f;
        // Update is called once per frame
        void Update()
        {
            //if (ScoreManager.isGameOver)
            //{
            //    return;
            //}

            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.forward * m_PanSpeed * Time.deltaTime, Space.World);
            }

            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(Vector3.back * m_PanSpeed * Time.deltaTime, Space.World);
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * m_PanSpeed * Time.deltaTime, Space.World);
            }

            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(Vector3.left * m_PanSpeed * Time.deltaTime, Space.World);
            }

            float m_ScrollInput = Input.GetAxis("Mouse ScrollWheel");
            Vector3 m_Position = transform.position;
            m_Position.y -= m_ScrollInput * m_ScrollSpeed * Time.deltaTime * 100;

            // keep camera within game bounds
            m_Position.y = Mathf.Clamp(m_Position.y, m_MinY, m_MaxY);
            m_Position.x = Mathf.Clamp(m_Position.x, m_MinX, m_MaxX);
            m_Position.z = Mathf.Clamp(m_Position.z, m_MinZ, m_MaxZ);

            transform.position = m_Position;


        }
    }
}


