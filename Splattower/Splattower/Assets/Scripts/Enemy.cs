﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{

    //path variables
    GameObject enemyPath;
    Transform targetNode;
    int nodeIndex = 0;
    int numberNodes;

    //enemy variables
    public float speed = 5.0f;
    public int health = 3;
    public int damage = 1;
    public int rewardMoney = 10;

    // Use this for initialization
    void Start()
    {
        //obtain path and number of nodes
        enemyPath = GameObject.Find("EnemyPath");
        numberNodes = enemyPath.transform.childCount;

    }

    void GetNextNode()
    {
        //obtain next node in path and update the node index
        targetNode = enemyPath.transform.GetChild(nodeIndex);
        nodeIndex++;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetNode == null)
        {
            //request next node in path
            GetNextNode();
        }

        Vector3 direction = targetNode.position - this.transform.localPosition; //direction for enemy to travel
        float travelDistance = speed * Time.deltaTime;  //distance enemy will cover each update

        if (direction.magnitude <= travelDistance)  //true when enemy reaches current target node
        {
            //request for next node in path to become the target
            if (nodeIndex < numberNodes)
            {
                targetNode = null;
            }
            else
            {
                //enemy has reached the goal!
                GameObject.FindObjectOfType<ScoreManager>().TakeDamage(damage);


                //always do last
                KillEnemy();
            }
            
        }
        else
        {
            //move enemy towards next node
            transform.Translate(direction.normalized * travelDistance, Space.World);

            //rotate gradually towards next node
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 10);

        }

    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            //enemy has lost all its health!
            GameObject.FindObjectOfType<ScoreManager>().AddMoney(rewardMoney);



            //always do last
            KillEnemy();
        }
    }

    void KillEnemy()
    {
        Destroy(gameObject);
    }
}
