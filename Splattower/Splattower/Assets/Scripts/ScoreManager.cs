﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public int health = 100;
    public int money = 500;

    public Text moneyText;
    public Text livesText;

    public void TakeDamage(int damage)
    {
        health -= damage;
        livesText.text = "Lives: " + health.ToString();
        if (health <= 0)
        {
            GameOver();
        }
    }

    public void AddMoney(int rewardMoney)
    {
        money += rewardMoney;
        moneyText.text = "Money: " + money.ToString();
    }

    public void GameOver()
    {
        Debug.Log("You Lose!");
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Use this for initialization
    void Start()
    {
        livesText.text = "Lives:"+ health.ToString();
        moneyText.text = "Money:"+ money.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //testing
        if (Input.GetKey(KeyCode.H))
        {
            TakeDamage(-1);
        }
    }
}
