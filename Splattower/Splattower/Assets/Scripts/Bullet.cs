﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    [SerializeField]
    public float speed = 15.0f;
    public int damage = 1;
    //public float lifeSpan = 10f;
    public Transform target;

    public GameObject PaintSplat;

    // Use this for initialization

    void KillBullet()
    {
        if (PaintSplat != null)
        {
            Instantiate(PaintSplat, new Vector3(this.transform.position.x, 0.01f, this.transform.position.z), Quaternion.Euler(0, Random.Range(0, 360), 0));
        }
        //do last
        Destroy(gameObject);
    }

    public void BulletHit()
    {
        target.GetComponent<Enemy>().TakeDamage(damage);
        KillBullet();
    }

    // Update is called once per frame
    void Update()
    {
        //lifeSpan -= Time.deltaTime;

        // if (target == null || lifeSpan <= 0)
        if (target == null)
        {
            //removes bullet if enemy no longer exists or the bullet has been active for too long
            KillBullet();
        }
        else
        {
            Vector3 direction = target.position - this.transform.localPosition;
            float travelDistance = speed * Time.deltaTime;

            if (direction.magnitude <= travelDistance)
            {
                BulletHit();
            }
            else
            {
                transform.Translate(direction.normalized * travelDistance, Space.World);
                Quaternion targetRotation = Quaternion.LookRotation(direction);
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 10);
            }
        }


    }
}
