﻿using UnityEngine;
using System.Collections;

public class DecalLife : MonoBehaviour
{

    public float lifeSpan = 5.0f;
    private bool isEnlarged = false;
    private Vector3 fullSize;
    public float initialSize = 0.5f;
    private Vector3 shrink;
    private Vector3 enlarge;

    // Use this for initialization
    void Start()
    {
        fullSize = transform.localScale; //full size of paint splat
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(initialSize, 1, initialSize));
        shrink = new Vector3(0.9f, 1, 0.9f);
        enlarge = new Vector3(1.2f, 1, 1.2f);
    }

    // Update is called once per frame
    void Update()
    {
        lifeSpan -= Time.deltaTime;


        if (lifeSpan <= 0)
        {
            Destroy(gameObject);
        }
        else if (lifeSpan < 0.5)
        {
            transform.localScale = Vector3.Scale(transform.localScale, shrink);
        }
        else if(transform.localScale.magnitude<fullSize.magnitude)
        {
            transform.localScale = Vector3.Scale(transform.localScale, enlarge);
        }


    }
}
