﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {
    public GameObject spawn;
    public GameObject enemy;
    public GameObject bossEnemy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("1"))
        {
            GameObject enemyGO = (GameObject)Instantiate(enemy, spawn.transform.position, spawn.transform.rotation);
        }

        if (Input.GetKeyDown("2"))
        {
           GameObject bossEnemyGO = (GameObject)Instantiate(bossEnemy, spawn.transform.position, spawn.transform.rotation);
        }
    }
}
