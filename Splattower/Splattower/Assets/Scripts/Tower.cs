﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
    //target acquisition
    Enemy nearestEnemy;
    private float distance = Mathf.Infinity;
    Transform turretTransform;
    Transform spawnTransform;
    Quaternion turretRotation;
    Vector3 direction;
    private float adjustmentAngle = 270;

    //tower turret variables
    public GameObject bulletPrefab;
    public float range = 10.0f;
    public float fireTimeDelay = 2.0f;
    private float fireTimeRemaining;

    // Use this for initialization
    void Start()
    {
        fireTimeRemaining = 0.0f;
        nearestEnemy = null;
        distance = Mathf.Infinity;
        turretTransform = transform.Find("Turret");
        spawnTransform = turretTransform.transform.Find("BulletSpawn");
    }

    // Update is called once per frame
    void Update()
    {

        Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

        CheckForEnemies();

        if (enemies.Length > 0)
        {
            FaceNearestEnemy(enemies);
        }
    }

    void CheckForEnemies()
    {
        if (nearestEnemy != null)
        {
            distance = (nearestEnemy.transform.position - this.transform.position).magnitude;
            if (distance > range)
            {
                nearestEnemy = null;
            }
        }
        else
        {
            NoTarget();
        }
    }

    void FaceNearestEnemy(Enemy[] enemies)
    {
        for (int e = 0; e < enemies.Length; e++)
        {
            float d = Vector3.Distance(this.transform.position, enemies[e].transform.position);
            if ((nearestEnemy == null || d < distance) && d < range)
            {
                nearestEnemy = enemies[e];
                distance = d;
            }
        }

        if (distance <= range)
        {
            direction = nearestEnemy.transform.position - this.transform.position;
            turretRotation = Quaternion.LookRotation(direction);
            turretTransform.rotation = Quaternion.Euler(adjustmentAngle, turretRotation.eulerAngles.y, 0);

            fireTimeRemaining -= Time.deltaTime;
            if (fireTimeRemaining <= 0)
            {
                fireTimeRemaining = fireTimeDelay;
                ShootTarget(nearestEnemy);
            }
        }
    }

    void NoTarget()
    {
        //no enemies in range
        nearestEnemy = null;
        distance = Mathf.Infinity;
        turretTransform.rotation = Quaternion.Euler(adjustmentAngle, turretTransform.rotation.eulerAngles.y + 2, 0);
    }

    void ShootTarget(Enemy e)
    {
        //shoot bullets from the barrel of the tower's turret
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, spawnTransform.transform.position, spawnTransform.transform.rotation);
        Bullet b = bulletGO.GetComponent<Bullet>();
        b.target = e.transform;
    }

}
